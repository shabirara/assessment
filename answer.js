// - create function to determine the given expression is correct or not
// - expression will be string of open bracket nor close bracket e.g `({[[()]]})`
// - your function should determine the expression is it correct or not
// - Don't use build in function
// - examples:
//   - `({[]})` => true
//   - `([][]{})`=> true
//   - `({)(]){[}` => false
//   - `[)()]` => false

function isTrue(param) {
    let stack = []
    for (let i = 0; i < param.length; i++) {
        let x = param[i]
        if (x == '(' || x == '{' || x == '[') {
            stack.push(x)
        } else {
            const check = stack.pop()
            switch (x) {
                case ')':
                    if (check == '{' || check == '[') {
                        return false
                    }
                    break
                case '}':
                    if (check == '(' || check == '[') {
                        return false
                    }
                    break

                case ']':
                    if (check == '{' || check == '(') {
                        return false
                    }
                    break

                default:
                    break
            }
        }

    }
    return stack.length == 0
}

console.log(isTrue('{[]}('));


console.log(isTrue('[][]{}'))


console.log(isTrue('{)(]){[}'))

console.log(isTrue('[)()]'))